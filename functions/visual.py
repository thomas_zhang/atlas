"""
DESCRIPTION

Written by Thomas Zhang: thomas@owler-inc.com on DATE.
"""

import igraph as ig 
import numpy as np 
import json
import random as rd
import forceatlas2 as fa

def make_colors(N):
    """ return a dictionary of rgb colors given the total number of groups"""
    r = lambda: rd.randint(0,255)
    cdic = {}
    seen = set()
    for i in range(N):
        color = (r(),r(),r())
        while color in seen:
            color = (r(),r(),r())
        cdic[i] = color
        seen.add(color)
    return cdic


def visual_json(g,nlst,elst,file_name = 'data.json'):
    print('visualization preparation starts')
    data = {}

    # adding attributes
    # add layouts
    A = np.asarray(g.get_adjacency().data)
    layout = fa.forceatlas2(A)
    nlst['x'] = [e[0] for e in layout]
    nlst['y'] = [e[1] for e in layout]
    nlst['degree'] = g.degree()
    # build color dictionary
    cdic = make_colors(max(nlst['group'])+1)
    print('analysis complete')

    # filtering attributes
    # filter nodes
    nlst = nlst[nlst['degree'] >= 10]
    nset = set(nlst['_id'].values)
    # filter edges
    elst = elst[elst['weight'] > 1000]
    elst = elst[elst['from'].isin(nset)]
    elst = elst[elst['to'].isin(nset)]

    # writing nodes
    ndic = {} # for group look up
    nodes = []
    for row in nlst.itertuples(index = False,name='Pandas'):
        ndic[str(getattr(row,'_1'))] = getattr(row,'group')# for group look up
        node = {}
        node['id'] = str(getattr(row,'_1'))
        node['label'] = getattr(row,'short_name')
        node['x'] = getattr(row,'x')
        node['y'] = getattr(row,'y')
        node['color'] = 'rgb'+str(cdic[getattr(row,'group')])
        node['size'] = getattr(row,'degree')
        node['attributes'] = {#'total revenue':getattr(row,'total_revenue'),
            #'total employees':getattr(row,'total_employees'),   
            'company name':str(getattr(row,'name')),
            #'industry':getattr(row,'industry'),   
            'group keywords':getattr(row,'keywords'),   
            'description':str(getattr(row,'description')),
            'input':getattr(row,'input')
            }   
        nodes.append(node)
    data['nodes'] = nodes
    print('done writing nodes')

    # writing edges
    edges = []
    for row in elst.itertuples(index = False,name='Pandas'):
        edge = {}
        s, t = str(getattr(row,'_1')), str((getattr(row,'to')))
        edge['id'] = getattr(row,'index')
        edge['source'] = s
        edge['target'] = t
        edge['size'] = 1.0
        edge['attributes'] = {}
        # add color
        c1, c2 = cdic[ndic[s]], cdic[ndic[t]]
        color = tuple((sum(x)//2 for x in zip(c1,c2)))
        edge['color'] = 'rgb'+str(color)
        edges.append(edge)
    data['edges'] = edges        
    print('done writing edges')

    json.dump(data,open(file_name,'w'),sort_keys=True,indent=4, separators=(',', ': '))
    return

if __name__=="__main__":
    print('built')
