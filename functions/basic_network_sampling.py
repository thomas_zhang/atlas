"""
Functions and methods for sampling subgraph from a large network.

Written by Thomas Zhang: thomas@owler-inc.com on Oct 10, 2017.
"""
import igraph as ig
import pandas as pd
import numpy as np

def node_fixed_weight(node, edgelist, tot_weight = 100000, thres = 1000):
	''' rooted on a node and depth first search its neighbors.
	node: the source node (INT object)
	edgelist: pandaframe of edgelists with weight
	tot_weight: total weights/crawling power we have. The higher the total weight, the further
	we can crawl.
	thres: This sets the maximal dissipation for neighbors with proximity 100000.
	--- cleaning might be needed.
	'''
	nodelist = set([])

	def dfs(node, edgelist, tot_weight,nodelist):
		if tot_weight >= 0 and node not in nodelist:
			nodelist.add(node)
			neighbor = edgelist[edgelist['from'] == node]
			for i in range(len(neighbor)):
				nei, cost = neighbor['to'].values[i], 100000 + thres - neighbor['weight'].values[i]
				dfs(nei, edgelist, tot_weight - cost,nodelist)

	dfs(node, edgelist,tot_weight,nodelist)

	return nodelist


def process_and_save(sample, nodelist, edgelist, save_path = '', company = 'COMPANY_NAME'):
	''' using the sampled subgraph to build graph and saving the data
	'''

	nlst = nodelist[nodelist['_id'].isin(sample)]
	elst = edgelist[edgelist['from'].isin(sample)]
	elst = elst[elst['to'].isin(sample)]

	# saving data to file
	nlst.to_csv(save_path+company+'_nlst.csv',index = False)
	elst.to_csv(save_path+company+'_elst.csv',index = False)
	return nlst, elst


def node_fixed_steps(node, edgelist, r = 2):
	nodelist = set([])
	visit = [node]

	ite = 0
	while ite <= r:
		temp =  []
		for i in visit:
			nodelist.add(i)
			if ite < r:
				nei = edgelist[edgelist['from'] == i]
				for j in nei['to']:
					if j not in nodelist:
						temp.append(j)
		visit = temp
		ite += 1
	return nodelist

def select_nodes(nlist,nodelist,edgelist):
	''' sample the nodes in a given list'''
	sample = set(nlist)
	for e in nlist:
		neighbor = edgelist[edgelist['from'] == e]['to'].values
		for node in neighbor:
			sample.add(node)
	return sample

def main():
    read_path = '/Users/thomas/Documents/owler/data/raw/'
    save_path = '/Users/thomas/Documents/owler/data/processed/'
    nodelist = pd.read_csv(read_path+'company_11212017.csv')
    edgelist = pd.read_csv(read_path+'cg_nov_edgelist.csv')
    print('read complete.')

    root = 103807
    sample = node_fixed_weight(root,edgelist,tot_weight = 200000)
    #sample = node_fixed_steps(root,edgelist, r = 2)
    #nlist = [12168089,704640,141717,12140521,12133520,4417541] #ai_6 companies
    #nlist = np.loadtxt('sf_inputlist.txt')
    #sample = select_nodes(nlist,nodelist,edgelist)
    print('sample complete, the sample size is ', len(sample))

    process_and_save(sample,nodelist,edgelist,save_path = save_path,company = 'dow_jones')
    print('data saved')
    return sample

if __name__=="__main__":
	print('build')
	#sample = main()

	"""
	nodes = pd.read_csv('ai_clarifai_nlst.csv')
	edges = pd.read_csv('ai_clarifai_elst.csv')

	g = build_undirected(nodes,edges,thres = 1000)
	g.write_pickle('ai_clarifai.pkl')
	"""
