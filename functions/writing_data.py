import pandas as pd
from collections import Counter
import re
import numpy as np

def writing_csv(nodes, filename = 'output.xlsx', size_thres = 0):
    # preparing
    group_dic = Counter(nodes['group']).most_common()
    #nodes = nodes.replace(np.nan,'',regex=True)
    nodes['name'] = [re.sub('[^0-9a-zA-Z]+'," ",e) for e in nodes['name']]
    nodes['short_name'] = [re.sub('[^0-9a-zA-Z]+'," ",e) for e in nodes['short_name']]
    nodes['description'] = [re.sub('[^0-9a-zA-Z]+'," ",e) for e in nodes['description']]
    # writing
    writer = pd.ExcelWriter(filename)
    outputnodes = nodes[nodes['input'] != 1]
    for e in group_dic:
        index, count = e[0], e[1]
        if count > 0:
            df = outputnodes[outputnodes['group'] == index]
            df = df.sort_values(by='person_pagerank',ascending=False)
            try:
                sheetname = df['keywords'].values[0]
                df.to_excel(writer, sheet_name = sheetname[:30],encoding='utf-8',index=False)
            except:
                pass
    writer.save()
    return


def main():
    return


if __name__ == '__main__':
    main()
    df = nlst
    df = df.drop(['index','industry'],axis=1)
    writing_csv(df)
