"""
basic name suggesting for clusters based on NLP
"""

# tf idf with sklearn
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from stop_words import get_stop_words
from nltk.stem import SnowballStemmer as snowstem
from nltk.tokenize import RegexpTokenizer
from collections import Counter
import numpy as np


def tfidf_naming(nodes):
    # preparing training data
    group_dic = Counter(nodes['group']).most_common()
    arr = nodes['description'].values
    for i,e in enumerate(arr):
        if type(e) != str:
            arr[i] = ''
    nodes['description'] = arr

    slist = ['' for _ in range(len(group_dic))]
    for e in group_dic:
        index, count = e[0], e[1]
        df = nodes[nodes['group'] == index]
        string = ' '.join(df['description'].values)
        slist[index] = string

    # filtering for each news
    tokenizer = RegexpTokenizer(r'\w+')
    # create English stop words list
    en_stop = get_stop_words('en')
    tf = TfidfVectorizer(analyzer='word', ngram_range=(1,3),
                         min_df = 0, stop_words = en_stop, sublinear_tf=True)

    X = tf.fit_transform(slist)
    features = tf.get_feature_names()
    worddic = {i:e for i,e in enumerate(features)}

    keywords = ['' for _ in range(len(group_dic))]
    for i in group_dic:
        index, count = i[0], i[1]
        e = X[index]
        arr = np.asarray(e.todense())
        arr = arr.reshape(X.shape[1],)
        args = np.argsort(arr)[::-1]
        #keywords.append([(arr[i],dic[i]) for i in args[:5]])
        words = [worddic[i] for i in args[:5]]
        keywords[index] = ','.join(words)
    return keywords

def main():
    return

if __name__ == '__main__':
    main()
