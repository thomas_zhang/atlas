"""
Sampling from large network based on methods proposed by Leskovec and Faloutsos:
http://www.stat.cmu.edu/~fienberg/Stat36-835/Leskovec-sampling-kdd06.pdf

Input files edgelist and nodelist are assumed to be in the form of
pandas.DataFrame

Written by Thomas Zhang: thomas@owler-inc.com on Oct 25, 2017
"""
import pandas as pd
import numpy as np
from scipy.stats import poisson
import igraph as ig
import random as rd

def build_graph(nodelist, edgelist):
    ''' build a igraph.Graph based on given edgelist'''
    nlist = nodelist['_id'].values
    ndic = {nlist[i]:i for i in range(len(nlist))}
    g = ig.Graph(directed = True)
    g.add_vertices(len(nlist))
    sou_list = edgelist['from'].values
    tar_list = edgelist['to'].values
    slist = []
    tlist = []
    for i in range(len(sou_list)):
        try:
            slist.append(ndic[sou_list[i]])
            tlist.append(ndic[tar_list[i]])
        except:
            continue
    elist = zip(slist,tlist)
    g.add_edges(elist)

    #fillings attributes
    for l in nodelist.keys():
        g.vs[l] = nodelist[l].values
    g.es['weight'] = edgelist['weight']
    return g

def build_undirected(nodelist, edgelist, thres = 1000):
    ''' build an undirected igraph.Graph based on given edgelist'''
    nlist = nodelist['_id'].values
    ndic = {nlist[i]:i for i in range(len(nlist))}
    g = ig.Graph(directed = False)
    g.add_vertices(len(nlist))
    sou_list = edgelist['from'].values
    tar_list = edgelist['to'].values
    wlist = edgelist['weight'].values
    slist = []
    tlist = []
    for i in range(len(sou_list)):
        if wlist[i] > thres:
            try:
                slist.append(ndic[sou_list[i]])
                tlist.append(ndic[tar_list[i]])
            except:
                continue
    elist = zip(slist,tlist)
    g.add_edges(elist)
    g.simplify()	

	#fillings attributes
    for l in nodelist.keys():
        g.vs[l] = nodelist[l].values
    g.es['weight'] = edgelist['weight']    
    return g

def random_node(nodelist,edgelist,p = 0.1):
    ''' function for sampling network based on random selecting nodes
    output:
    --------
    sample : DataFrame
    '''
    size = poisson.rvs(int(len(nodelist)*p))
    nsample = nodelist.sample(size)
    nlist = nsample['_id'].values
    esample = edgelist[edgelist['from'].isin(nlist)]
    esample = esample[esample['to'].isin(nlist)]
    return nsample,esample

def random_edge(nodelist,edgelist,p = 0.1):
    ''' function for sampling network based on random edge selections'''
    size = poisson.rvs(int(len(edgelist)*p))
    esample = edgelist.sample(size)
    nlist = set(esample['from'].values)&set(esample['to'].values)
    nsample = nodelist[nodelist['_id'].isin(nlist)]
    return nsample,esample

def build_adjlist(nodelist,edgelist):
    ''' build adjacency list from edgelist'''
    adjlist = {k:set([]) for k in nodelist['_id'].values}
    flist = edgelist['from'].values
    tlist = edgelist['to'].values
    for i in range(len(flist)):
            adjlist[flist[i]].add(tlist[i])
    degree = {k:len(v) for k,v in adjlist.iteritems()} #outdegree
    adjlist = {k:list(v) for k,v in adjlist.iteritems()}
    return adjlist, degree

def random_walk(nodelist,edgelist,alpha = 0.15,step = 10,p = 0.1):
    ''' sample node based on random walk'''
    adjlist, degree = build_adjlist(nodelist,edgelist)

    #random walk sampling
    root = rd.choice(nodelist['_id'].values)
    root = 100651
    n = len(nodelist)
    chain = -1*np.ones(step*n)
    chain[0] = root
    rv = np.random.rand(step*n)
    nlist = set([])
    for i in range(1,n*step):
        #resample if sample size does not meet minimum requirement
        if i % n == 0:
            print(i, ' current step')
            if len(nlist) < p*n:
                root = rd.choice(nodelist['_id'].values)

        #random walk
        if rv[i] >= 1 - alpha or degree[chain[i-1]] == 0:
            chain[i] = root
        else:
            r = (1-alpha)/degree[chain[i-1]]
            chain[i] = adjlist[chain[i-1]][int(rv[i]/r)]
        nlist.add(chain[i])

    #sample graph
    nsample = nodelist[nodelist['_id'].isin(nlist)]
    esample = edgelist[edgelist['from'].isin(nlist)]
    esample = esample[esample['to'].isin(nlist)]
    return nsample,esample

def nonbacktracking_random_walk(nodelist,edgelist,alpha = 0.15,step = 10,p = 0.1):
    ''' sample node based on nonbacktracking random walk'''
    adjlist, degree = build_adjlist(nodelist,edgelist)

    #random walk sampling
    root = rd.choice(nodelist['_id'].values)
    n = len(nodelist)
    chain = -1*np.ones(step*n)
    chain[0] = root
    rv = np.random.rand(step*n)
    nlist = set([])
    for i in range(1,n*step):
        #resample if sample size does not meet minimum requirement
        if i % n == 0:
            print(i, ' current step')
            if len(nlist) < p*n:
                root = rd.choice(nodelist['_id'].values)

        # non-backtracking random walk
        if rv[i] >= 1 - alpha or degree[chain[i-1]] == 0:
            chain[i] = root
        else:
            r = (1-alpha)/degree[chain[i-1]]
            chain[i] = adjlist[chain[i-1]][int(rv[i]/r)]
        nlist.add(chain[i])

    #sample graph
    nsample = nodelist[nodelist['_id'].isin(nlist)]
    esample = edgelist[edgelist['from'].isin(nlist)]
    esample = esample[esample['to'].isin(nlist)]
    return

def main():
    read_path = '/Users/thomas/Documents/owler/data/raw/'
    save_path = '/Users/thomas/Documents/owler/data/processed/'
    nodelist = pd.read_csv(read_path+'company_list.csv')
    edgelist = pd.read_csv(read_path+'company_edges.csv')
    print('read complete.')

    #choose your sampling method here
    nsample, esample = random_walk(nodelist,edgelist,step = 1)
    nsample.to_csv(save_path+'randomwalk_nlst.csv',index = False)
    esample.to_csv(save_path+'randomwalk_elst.csv',index = False)
    print('sizes of the samples are:')
    print('numner of nodes: ', len(nsample))
    print('number of edges: ', len(esample))
    print('save complete.')
    return nsample, esample

if __name__ == '__main__':
    #nlist, esample = main()
    print('build')
