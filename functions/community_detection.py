"""
basic community modules for atlas
"""
import igraph as ig


def louvain(g,level = 'min'):
    glevels = g.community_multilevel(return_levels = True)
    if level == 'max':
        return glevels[-1].membership
    return glevels[0].membership


def main():
    return


if __name__ == '__main__':
    main()
