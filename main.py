"""
main scripts for running ATLAS process for company expansion and list generations.

Original Input:
    - a csv file with a list of companies
    note: the original input needs to go through a company matching process using existing API.

Input:
    - a csv file with a list of companies represented by company id

Output:
    - csv file with expanded set of companies with clustering, firmographic data etc,.

---
Written by Thomas Zhang: thomas@owler-inc.com on December 18, 2017.

---
To-do list:
    finish visualization code
"""

import pandas as pd
import json
import sys
import igraph as ig
import numpy as np
import time as t

sys.path.append('functions')
from basic_network_sampling import select_nodes, process_and_save
from leskovec_sampling import build_undirected, build_graph
from community_detection import louvain
from cluster_naming import tfidf_naming
from writing_data import writing_csv
from visual import visual_json

start = t.time()

def main():
    print('process begins')    
    # read data, in production, replace with connection to database and plug in data acquisitions for firmographic data.
    nodes = pd.read_csv('data/company_12182017.csv')
    edges = pd.read_csv('data/cg_12182017_edgelist.csv')
    input_list = pd.read_csv('cision_input.csv')
    print('data read complete')

    # sampling nodes
    nlist = input_list['company_id'].values
    sample = select_nodes(nlist,nodes,edges)
    nlst, elst = process_and_save(sample,nodes,edges,save_path='data/',company='cision')
    nlst = nlst.reset_index()
    elst = elst.reset_index()
    print('sampling complete')

    ### analyzing network
    # clustering and community detection
    g = build_undirected(nlst,elst)
    glist = louvain(g,level = 'min')
    nlst['group'] = glist
    # flag as input or not
    lst = np.zeros(len(nlst))
    for i in range(len(nlst)):
        if nlst['_id'].values[i] in nlist:
            lst[i] = 1
    nlst['input'] = lst
    # centrality measure
    gdir = build_graph(nlst,elst)
    person = np.ones(len(nlst))
    person += 49*nlst['input']
    pr = gdir.personalized_pagerank(damping = 0.15,reset = person,weights = gdir.es['weight'])
    nlst['person_pagerank'] = [int(100000*e/max(pr)) for e in pr]
    # adding keywords
    keywords = tfidf_naming(nlst)
    nlst['keywords'] = [keywords[i] for i in nlst['group'].values]
    print('analyzing complete')
    
    ### writing to csv
    writing_csv(nlst)        
    print('done writing')

    ### visualization preparation
    visual_json(g,nlst,elst,file_name = 'data.json')
    print('visualization data saved')
    return

if __name__ == "__main__":
    main()
    print('here', t.time() - start)
